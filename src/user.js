// User data model
class User {
    constructor(name, email) {
        this.id = auth_user_id();
        this.name = name;
        this.email = email;
        this.role = "user";
    }
}

// A psudo authenticator
// returns authenticated user ID
function auth_user_id() {
    return 2;
}

module.exports = { User };
